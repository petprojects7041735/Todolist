import React, {SyntheticEvent, useEffect, useState} from 'react';
import axios from "axios";
import {ITask} from "../types/tasks";

const Home = () => {
    const [name, setName] = useState('');
    const [textOfTask, settextOfTask] = useState('');
    const [deadline, setdeadline] = useState('');
    const [tasks, setTasks] = useState<ITask[]>();
    const [content,setContent] = useState()


    const submit = async (e: SyntheticEvent) => {
        e.preventDefault    ();

        const response = await fetch('http://localhost:8080/api/create_task', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                textOfTask,
                deadline,
            })
        });
    }




    const callTasks =  async () => {
                const response = await fetch('http://localhost:8080/api/tasks', {
                    headers: {'Content-Type': 'application/json'},
                    credentials: 'include',
                });

                const jsonResponse = await response.json();
                  console.log(jsonResponse)

        return JSON.stringify(jsonResponse);
    };

    function RenderResult() {
        const [apiResponse, setApiResponse] = useState("*** now loading ***");

        useEffect(() => {
            callTasks().then(
                result => setApiResponse(result));
        },[]);

        return(
            <div>

                <h4>{apiResponse}</h4>
            </div>
        );
    };






    return (
        <div>
            <div>
            {localStorage.getItem("Username") ? 'Hi ' + localStorage.getItem("Username") : 'You are not logged in'}
            </div>
            <div>
            <form onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">Create new task</h1>


                <input type="task" className="form-control"  placeholder="Buy house" required
                       onChange={e=>settextOfTask(e.target.value)}
                />



                <input type="deadline" className="form-control"  placeholder="1 October" required
                       onChange={e=>setdeadline(e.target.value)}
                />

                <button className="btn btn-primary w-100 py-2" type="submit">Create task!</button>
            </form>

            </div>
          <RenderResult/>


        </div>


    );
};

export default Home;