package http

import (
	"TasksProject2/internal/users"
	"TasksProject2/internal/users/usecase"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"net/http"
	"time"
)

type UserHandler struct {
	userUc *usecase.UserUsecase
}

func NewUserHandler(userUc *usecase.UserUsecase) *UserHandler {
	return &UserHandler{userUc: userUc}
}

func (h *UserHandler) Register(ctx *fiber.Ctx) error {
	user := users.User{}
	err := ctx.BodyParser(&user)
	if err != nil {
		return err
	}
	fmt.Println("i am here")
	err = h.userUc.CreateUser(&user)
	fmt.Println("i am here2")

	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(
			&fiber.Map{"message": "could not create user"})
		return err
	}

	fmt.Println("i am here3")
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"message": "User has been added"})
	return nil
}

func (h *UserHandler) Login(ctx *fiber.Ctx) error {
	var data users.User
	err := ctx.BodyParser(&data)
	if err != nil {
		return err
	}
	userToken, err := h.userUc.Authorize(&data)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest)
		return ctx.JSON(fiber.Map{"message": err})

	}

	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    userToken,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	}
	ctx.Cookie(&cookie)

	return ctx.JSON(fiber.Map{
		"message": "success",
	})
}

func (h *UserHandler) User(ctx *fiber.Ctx) error {
	cookie := ctx.Cookies("jwt")

	user, err := h.userUc.GetUserFromCookie(cookie)
	if err != nil {
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(fiber.Map{
			"message": "unauthenticated",
		})
	}
	fmt.Println(user)

	return ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"name": user})
}

func (h *UserHandler) Logout(ctx *fiber.Ctx) error {

	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}
	ctx.Cookie(&cookie)
	return ctx.JSON(fiber.Map{"message": "success"})
}
