package http

import (
	"github.com/gofiber/fiber/v2"
)

type TaskRoutes struct {
	taskHandlers *TaskHandler
}

func NewTaskRoutes(taskHandlers *TaskHandler) *TaskRoutes {
	return &TaskRoutes{taskHandlers: taskHandlers}
}

func (taskRoutes *TaskHandler) SetupRoutes(app *fiber.App) {
	api := app.Group(`/api`)
	api.Post(`/create_task`, taskRoutes.CreateTask)
	api.Delete("/delete/:id", taskRoutes.DeleteTaskByID)
	api.Get("/get_tasks/:id", taskRoutes.GetTaskById)
	api.Get("/tasks", taskRoutes.GetAllTasks)
	api.Patch("finish_task/:id", taskRoutes.FinishTaskByID)

}
