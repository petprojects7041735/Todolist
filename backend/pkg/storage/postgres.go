package storage

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)
import "fmt"

type Config struct {
	Host     string
	Port     string
	Password string
	User     string
	DBName   string
	SSLMode  string
}

func NewConnection(config *Config) *sqlx.DB {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", config.Host, config.User, config.Password, config.DBName, config.Port, config.SSLMode)
	db := sqlx.MustConnect("postgres", dsn)

	return db
}
