package main

import (
	"TasksProject2/internal/httpServer"
	"TasksProject2/pkg/storage"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func main() {

	config := &storage.Config{
		Host:     "localhost",
		Port:     "5432",
		Password: "1",
		User:     "Alex",
		SSLMode:  "disable",
		DBName:   "fiber_final",
	}

	MyServer := httpServer.NewServer()
	fmt.Println("point")
	err := MyServer.Run(config)
	if err != nil {
		log.Fatal("Could not start the server")
	}

}

// Когда мы получаем в параметрах структуру, действия внутри фунуции не меняют эту структуру.
// Когда мы получаем в параметрах ссылку на стукрутру, мы изменеям поля структуры, которую передали
