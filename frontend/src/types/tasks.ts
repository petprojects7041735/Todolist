export interface ITask{
    id: number;
    author: string;
    textOfTask: string;
    createdAt: string;
    deadline: string;
    done: boolean;
}