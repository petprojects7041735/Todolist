import React, { useState} from 'react';
import './App.css';
import Login from "./Pages/Login";
import Nav from "./components/Nav";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./Pages/Home";
import Register from "./Pages/Register";



function App() {

    const [name, setName] = useState(' ');


  return (
    <div className="App">

        <BrowserRouter>
            <Nav/>

            <main className="form-signin">
                <Routes>
                 <Route path="/" element={<Home/>}/>
                    <Route path="/login" element={<Login/>}/>
                 <Route path="/register" element={<Register/>}/>
                 </Routes>


            </main>
        </BrowserRouter>
    </div>

  );

}

export default App;
