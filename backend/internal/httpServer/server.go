package httpServer

import (
	TasksHttp "TasksProject2/internal/tasks/delivery/http"
	TaskRepository "TasksProject2/internal/tasks/repository"
	TaskUsecase "TasksProject2/internal/tasks/usecase"
	UsersHttp "TasksProject2/internal/users/delivery/http"
	UsersRepository "TasksProject2/internal/users/repository"
	UsersUsecase "TasksProject2/internal/users/usecase"
	"TasksProject2/pkg/storage"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

var schema = `
CREATE TABLE IF NOT EXISTS tasks_sqlx (
    id serial primary key,
    username text,
    text_of_task text,
    done boolean,
    deadline text,
    created_at text
);

CREATE TABLE IF NOT EXISTS users_sqlx (
    name text,
    email text primary key,
    password text
)`

type Server struct {
	httpServer *fiber.App
}

func NewServer() *Server {
	return &Server{httpServer: fiber.New()}
}

func (s *Server) Run(cfg *storage.Config) error {
	db := storage.NewConnection(cfg)
	db.MustExec(schema)

	s.httpServer.Use(cors.New(cors.Config{
		AllowOriginsFunc: func(origin string) bool {
			return true
		},
		AllowCredentials: true,
	}))

	userRepo := UsersRepository.NewUserRepository(db)
	userUsecase := UsersUsecase.NewUserUsecase(userRepo)
	userhandlers := UsersHttp.NewUserHandler(userUsecase)

	userhandlers.SetupRoutes(s.httpServer)

	taskRepo := TaskRepository.NewTaskRepository(db)
	taskUsecase := TaskUsecase.NewTaskUsecase(taskRepo)
	taskhandlers := TasksHttp.NewTaskHandler(taskUsecase)

	taskhandlers.SetupRoutes(s.httpServer)

	return s.httpServer.Listen("localhost:8080")
}
