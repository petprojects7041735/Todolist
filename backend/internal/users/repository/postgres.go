package repository

import (
	"TasksProject2/internal/users"
	"fmt"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
)

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}

}

func (r *UserRepository) GetUserByEmail(email string) (users.User, error) {
	var user users.User
	err := r.db.Get(&user, "SELECT * FROM users_sqlx where email = $1 LIMIT 1", email)
	fmt.Println("err=", err)
	if err != nil {
		fmt.Println(err)
		return users.User{}, err
	}

	return user, nil
}

func (r *UserRepository) CreateUser(user *users.User) error {

	crypted, _ := bcrypt.GenerateFromPassword([]byte(user.Password), 14)
	user.Password = string(crypted)

	_, err := r.db.Exec("INSERT INTO users_sqlx (name, email, password) VALUES ($1,$2,$3)", user.Name, user.Email, user.Password)

	if err != nil {
		return err
	}

	return nil
}
