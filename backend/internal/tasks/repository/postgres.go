package repository

import (
	"TasksProject2/internal/tasks"
	"fmt"
	"github.com/jmoiron/sqlx"
)

type TaskRepository struct {
	db *sqlx.DB
}

func NewTaskRepository(db *sqlx.DB) *TaskRepository {
	return &TaskRepository{db: db}
}

func (r TaskRepository) CreateTaskInRepo(task *tasks.Task) error {

	_, err := r.db.Exec("INSERT INTO tasks_sqlx (username, text_of_task, done, deadline, created_at) VALUES ($1,$2,$3,$4,$5)", task.Author, task.TextOfTask, task.Done, task.Deadline, task.CreatedAt)
	if err != nil {
		return err
	}
	return nil
}

func (r TaskRepository) DeleteTaskByIdInRepo(id string) error {

	_, err := r.db.Exec("DELETE FROM tasks_sqlx where id = $1", id)
	if err != nil {
		return err
	}

	return nil

}

func (r TaskRepository) GetTaskByIdFromRepo(id string) (tasks.Task, error) {
	var task tasks.Task
	err := r.db.Select(task, "SELECT FROM tasks_sqlx where id = $1 LIMIT 1", id)
	if err != nil {
		return tasks.Task{}, err
	}

	return task, nil

}

func (r TaskRepository) GetAllTasksFromRepo(email string) ([]tasks.Task, error) {
	var dest []tasks.Task

	fmt.Println("here")
	err := r.db.Select(&dest, "SELECT * FROM tasks_sqlx where username = $1", email)
	if err != nil {
		return nil, err
	}
	fmt.Println("here2")
	fmt.Println(dest)
	return dest, nil

}

func (r TaskRepository) FinishTaskInRepo(id string) error {

	_, err := r.db.Exec("UPDATE tasks_sqlx SET done = TRUE WHERE id = $1", id)
	if err != nil {
		return err

	}

	return nil

}
