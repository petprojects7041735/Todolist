package usecase

import (
	"TasksProject2/internal/users"
	"TasksProject2/internal/users/repository"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"time"
)

const SecretKey = "Secrer"

type UserUsecase struct {
	userRepository *repository.UserRepository
}

func NewUserUsecase(userRepository *repository.UserRepository) *UserUsecase {
	return &UserUsecase{userRepository: userRepository}
}

func (u *UserUsecase) CreateUser(user *users.User) error {
	// Выполните все необходимые проверки и бизнес-логику перед созданием пользователя
	_, err := u.userRepository.GetUserByEmail(user.Email)
	if err == nil {
		return errors.New("User exist")
	}
	fmt.Println("checking complet")
	// Создание пользователя
	err = u.userRepository.CreateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func (u *UserUsecase) Authorize(user *users.User) (Token string, err error) {

	var userfromdb users.User
	userfromdb, err = u.userRepository.GetUserByEmail(user.Email)
	fmt.Println(err)
	if err != nil {
		return "", err
	}
	fmt.Println("connecting")
	err = bcrypt.CompareHashAndPassword([]byte(userfromdb.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    userfromdb.Email,
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	})

	token, err := claims.SignedString([]byte(SecretKey))
	if err != nil {

		return "", err
	}

	return token, nil

}

func (u *UserUsecase) GetUserFromCookie(cookie string) (UserName string, err error) {

	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return "", errors.New("unauthenticated")
	}
	claims := token.Claims.(*jwt.StandardClaims)
	var UserFromDb users.User
	UserFromDb, err = u.userRepository.GetUserByEmail(claims.Issuer)
	if err != nil {
		return "", errors.New("user not found")
	}
	return UserFromDb.Name, nil

}

// Выполните все необходимые проверки и бизнес-логику перед получением пользователя
