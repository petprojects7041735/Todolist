package http

import (
	"github.com/gofiber/fiber/v2"
)

type UserRoutes struct {
	userHandlers *UserHandler
}

func NewUserRoutes(userHandlers *UserHandler) *UserRoutes {
	return &UserRoutes{userHandlers: userHandlers}
}

func (userRoutes *UserHandler) SetupRoutes(app *fiber.App) {
	api := app.Group(`/api`)
	api.Post("/register", userRoutes.Register)
	api.Post("/login", userRoutes.Login)
	api.Get("/user", userRoutes.User)
	api.Post("/logout", userRoutes.Logout)
}
