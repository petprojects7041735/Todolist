package http

import (
	"TasksProject2/internal/tasks"
	"TasksProject2/internal/tasks/usecase"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type TaskHandler struct {
	taskUc *usecase.TaskUsecase
}

func NewTaskHandler(taskUc *usecase.TaskUsecase) *TaskHandler {

	return &TaskHandler{taskUc: taskUc}
}

func (h TaskHandler) CreateTask(ctx *fiber.Ctx) error {
	cookie := ctx.Cookies("jwt")

	task := tasks.Task{}
	err := ctx.BodyParser(&task)
	if err != nil {
		ctx.Status(http.StatusUnprocessableEntity).JSON(
			&fiber.Map{"message": "request failed"})
		return err
	}

	err = h.taskUc.AddTask(&task, cookie)
	if err != nil {
		ctx.Status(http.StatusInternalServerError).JSON(
			&fiber.Map{"message": "internal error"})
		return err

	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"message": "Task created successfully",
	})
	return nil
}

func (h TaskHandler) DeleteTaskByID(ctx *fiber.Ctx) error {

	id := ctx.Params("id")
	fmt.Println(id)
	if id == "" {
		ctx.Status(http.StatusInternalServerError).JSON(&fiber.Map{
			"message": "id cannot be empty"})
		return nil

	}
	err := h.taskUc.DeleteTaskById(id)
	if err != nil {
		ctx.Status(http.StatusInternalServerError).JSON(
			&fiber.Map{"message": err})
		return err

	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"message": "Task deleted successfully",
	})
	return nil
}

func (h TaskHandler) GetTaskById(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	if id == "" {
		ctx.Status(http.StatusInternalServerError).JSON(&fiber.Map{"message": "id cannot be empty"})
		return nil
	}
	task, err := h.taskUc.GetTaskById(id)
	if err != nil {
		ctx.Status(http.StatusBadRequest).JSON(&fiber.Map{"message": "could not get the task"})
		return err
	}

	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"message": "Task id fetched succeccfully",
		"data":    task})
	return nil
}

func (h TaskHandler) GetAllTasks(ctx *fiber.Ctx) error {
	token := ctx.Cookies("jwt")

	taskModels, err := h.taskUc.GetAllTasks(token)
	if err != nil {
		return err
	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"data": taskModels,
	})
	return nil

}

func (h TaskHandler) FinishTaskByID(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	fmt.Println(id)
	if id == "" {
		ctx.Status(http.StatusInternalServerError).JSON(&fiber.Map{"message": "id cannot be empty"})
		return nil
	}
	err := h.taskUc.FinishTaskById(id)
	if err != nil {
		return err

	}
	ctx.Status(http.StatusOK).JSON(&fiber.Map{
		"message": "Task is finished",
	})
	return nil

}
