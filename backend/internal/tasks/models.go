package tasks

type Task struct {
	ID         int     `json:"id" db:"id"`
	Author     *string `json:"author" db:"username"`
	TextOfTask *string `json:"textOfTask" db:"text_of_task"`
	CreatedAt  *string `json:"createdAt" db:"created_at"`
	Deadline   *string `json:"deadline" db:"deadline"`
	Done       bool    `json:"done" db:"done"`
}
