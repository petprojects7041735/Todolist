package usecase

import (
	"TasksProject2/internal/tasks"
	"TasksProject2/internal/tasks/repository"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

const SecretKey = "Secrer"

type TaskUsecase struct {
	taskRepository *repository.TaskRepository
}

func NewTaskUsecase(taskRepository *repository.TaskRepository) *TaskUsecase {
	return &TaskUsecase{taskRepository: taskRepository}
}

func (u TaskUsecase) AddTask(task *tasks.Task, cookie string) error {
	fmt.Println("ff")
	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return errors.New("unauthenticated")
	}
	claims := token.Claims.(*jwt.StandardClaims)

	CreationTime := time.Now().Format("01-02-2006 15:04:05")
	task.CreatedAt = &CreationTime
	task.Author = &claims.Issuer
	fmt.Println("ss")

	err = u.taskRepository.CreateTaskInRepo(task)
	if err != nil {
		return err
	}
	return nil
}

func (u TaskUsecase) DeleteTaskById(id string) error {

	err := u.taskRepository.DeleteTaskByIdInRepo(id)
	if err != nil {
		return err

	}
	return nil
}

func (u TaskUsecase) GetTaskById(id string) (tasks.Task, error) {
	var task tasks.Task
	task, err := u.taskRepository.GetTaskByIdFromRepo(id)
	if err != nil {
		return task, err
	}
	return task, nil
}

func (u TaskUsecase) GetAllTasks(tokencookie string) ([]tasks.Task, error) {
	//Мы передаем в репу ссылку "запиши вот по этому адесу" если ошибку при этом не выдало,
	//значит все записало, можем обращаться по этому адресу и не нужно возвращать весь массив данных
	fmt.Println("StartSearchinUc")

	token, err := jwt.ParseWithClaims(tokencookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return []tasks.Task{}, errors.New("unauthenticated")
	}
	claims := token.Claims.(*jwt.StandardClaims)

	AllTasks, err := u.taskRepository.GetAllTasksFromRepo(claims.Issuer)
	if err != nil {
		return AllTasks, errors.New("couldn't find the tasks")

	}
	return AllTasks, nil
}

func (u TaskUsecase) FinishTaskById(id string) error {

	err := u.taskRepository.FinishTaskInRepo(id)
	fmt.Println(err)
	if err != nil {
		return errors.New("couldn't complete the task")
	}
	return nil
}
